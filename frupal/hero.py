class Hero:
    # Initialize the hero
    def __init__(self, name, money, energy):
        self.__name__ = name
        self.__money__ = int(money)
        self.__energy__ = int(energy)
        self.__jewels__ = 0
        self.__tools__ = {}
        self.__location_x__ = 1
        self.__location_y__ = 1

    # Adds energy (for store bought energy)
    def add_energy(self, energy):
        self.__energy__ += int(energy)
        return self.__energy__

    def check_energy(self):
        return self.__energy__

    def check_jewels(self):
        return self.__jewels__

    def check_money(self):
        return self.__money__

    def sub_energy(self, energy):
        self.__energy__ -= int(energy)
        return self.__energy__

    # If we maybe want to increase the money the hero has
    def add_money(self, money):
        self.__money__ += int(money)
        return self.__money__

    def add_jewels(self, jewels):
        self.__jewels__ += int(jewels)
        return self.__jewels__

    def get_location(self):
        return self.__location_x__, self.__location_y__

    # Display for after every movement
    def short_display(self):
        print('Energy: ' + str(self.__energy__) + ' | Money: $'
              + str(self.__money__) + ' | Jewels: ' + str(self.__jewels__))

    # A more detailed display if the user wants
    # to know more information i.e. tools
    def display(self):
        print('Energy: ' + str(self.__energy__) + ' | Money: $'
              + str(self.__money__) + ' | Jewels: ' + str(self.__jewels__)
              + ' | Location: (' + str(self.__location_x__) + ","
              + str(self.__location_y__) + ")")
        if self.__tools__:
            my_tools = [tool['name'].capitalize()
                        for tool in self.__tools__.values()]
            print("Tools: ", end='')
            print(", ".join(my_tools))
        else:
            print("Tools: You have no Tools.")

    # Updates all stats after a movement
    def update_stats(self, energy, money, jewels, x, y):
        self.__energy__ -= int(energy)
        self.__money__ += int(money)
        self.__jewels__ += int(jewels)
        self.__location_x__ = int(x)
        self.__location_y__ = int(y)

    # Buy tool from the shop
    # Returns true if purchased, false if there were not enough funds
    def buy_tool(self, tool_to_add):
        price = tool_to_add['price']
        if price <= self.__money__:
            self.__tools__[tool_to_add['name'].lower().replace(' ', '_')] = \
                tool_to_add
            self.__money__ -= price
            return True
        else:
            return False

    def buy_power_bar(self, power_bar):
        price = power_bar['price']
        if price <= self.__money__:
            self.__money__ -= price
            self.__energy__ += int(power_bar['energy_saved'])
            return True
        else:
            return False

    # Returns all information that may be needed by game engine in a dictionary
    def get_hero_details(self):
        details = {
            'energy': self.__energy__,
            'money': self.__money__,
            'tools': self.__tools__,
            'jewels': self.__jewels__,
            'location': (self.__location_x__, self.__location_y__)
        }
        return details

    # Move functions, returns True if success, else returns false
    def move(self, x, y):
        self.__location_y__ = y
        self.__location_x__ = x

    def add_jewel(self):
        self.__jewels__ += 1

    def has_boat(self):
        return True if 'boat' in self.__tools__.keys() else False

    def has_binoculars(self):
        return True if 'binoculars' in self.__tools__.keys() else False

    def get_tool(self, tool):
        return self.__tools__[tool] if tool in self.__tools__.keys() else {}
