from map import Map
from settings import Settings
from hero import Hero
from map import InvalidMapSquare
from store import Store
from pyfiglet import Figlet


def main():
    my_settings = Settings()
    f = Figlet(font='slant')

    while True:
        print(f.renderText('Welcome to Frupal'))
        print(
            "\nThe Magical Jewel-Seeking Game!\n"
            "1. Start\n"
            "2. Settings\n"
            "3. Exit\n")
        reply = input("What would you like to do (1-3)?: ")
        if reply == "1":
            play(my_settings)
        elif reply == "2":
            my_settings.settings_menu()
        elif reply == "3":
            print("Thanks for playing!")
            exit()
        else:
            print("Not a valid choice. Try again: ")


def play(my_settings):
    my_map = Map(size=int(my_settings.map_size),
                 jewel_qty=int(my_settings.jewels),
                 obstacle_qty=int(my_settings.density),
                 obstacle_dict=my_settings.obstacles,
                 power_bar_qty=int(my_settings.powerbar_squares),
                 squares_with_money=int(my_settings.money_squares))
    my_hero = Hero("Guy", my_settings.money, my_settings.energy)
    my_store = Store(my_settings.tools)
    x = 1
    y = 1

    f = Figlet(font='slant')
    print(f.renderText('Time to FRUPAL!!'))
    print(f"There are {my_settings.jewels} magic jewels on the map to find!")
    print(f"You are playing on {my_settings.difficulty} level "
          f"which means there are {my_settings.density} obstacles on the map.")
    print("Find all the jewels to win the game!")
    print("Careful with your money and energy..."
          "If you run out of both, you lose!\n")
    print("Map: B = Bog, M = Mountain, P = Plain, L = Lake")
    print("Movement: (WASD) - Press W = North (Up), S = South (Down), D = East (Right), and A = West (Left)")
    print("Hint: Type 'L' at each move to survey terrain and obstacles so you can make educated moves.")
    print("Hint: Buy tools to get past obstacles and conserve energy.")
    print("\nHero Information:")
    my_hero.display()
    sq_details = my_map.move_into_square(x, y)
    if sq_details['jewel']:
        print("Lucky you! You started on a jewel.")
        update_jewels(my_hero, my_settings.jewels)
    power_bar_cost = int(my_settings.tools['power_bar']['price'])

    user_input = ''

    movement_options = {
        'up': {'w'},
        'down': {'s'},
        'left': {'a'},
        'right': {'d'},
        'look': {'l', 'lookaround'},
        'store': {'store', 'shop'},
        'character': {'char', 'character', 'c', 'status'},
        'win': {'win', 'jane massey mode'},
        'quit': {'exit', 'q', 'quit'}
    }

    input("\nPress Enter to begin. ")
    my_map.display()
    while user_input.lower() not in movement_options['quit']:
        user_input = print_instructions()
        if user_input.lower() in movement_options['up']:
            try:
                sq_details = my_map.check_square(x, y + 1)
                if not check_move(my_hero, sq_details, my_map, my_store, power_bar_cost):
                    continue
                sq_details = my_map.move_into_square(
                    x, y + 1, my_hero.has_binoculars())
                my_map.display()
                y += 1
                my_hero.move(x, y)
                play_square(my_settings.jewels, my_hero, sq_details)
                my_hero.short_display()
            except InvalidMapSquare:
                my_map.display()
                print('You may not move North.')
        elif user_input.lower() in movement_options['down']:
            try:
                sq_details = my_map.check_square(x, y - 1)
                if not check_move(my_hero, sq_details, my_map, my_store, power_bar_cost):
                    continue
                sq_details = my_map.move_into_square(
                    x, y - 1, my_hero.has_binoculars())
                my_map.display()
                y -= 1
                my_hero.move(x, y)
                play_square(my_settings.jewels, my_hero, sq_details)
                my_hero.short_display()
            except InvalidMapSquare:
                my_map.display()
                print('You may not move South.')
        if user_input.lower() in movement_options['left']:
            try:
                sq_details = my_map.check_square(x - 1, y)
                if not check_move(my_hero, sq_details, my_map, my_store, power_bar_cost):
                    continue
                sq_details = my_map.move_into_square(
                    x - 1, y, my_hero.has_binoculars())
                my_map.display()
                x -= 1
                my_hero.move(x, y)
                play_square(my_settings.jewels, my_hero, sq_details)
                my_hero.short_display()
            except InvalidMapSquare:
                my_map.display()
                print('You may not move West.')
        elif user_input.lower() in movement_options['right']:
            try:
                sq_details = my_map.check_square(x + 1, y)
                if not check_move(my_hero, sq_details, my_map, my_store, power_bar_cost):
                    continue
                sq_details = my_map.move_into_square(
                    x + 1, y, my_hero.has_binoculars())
                my_map.display()
                x += 1
                my_hero.move(x, y)
                play_square(my_settings.jewels, my_hero, sq_details)
                my_hero.short_display()
            except InvalidMapSquare:
                my_map.display()
                print('You may not move East.')
        elif user_input.lower() in movement_options['store']:
            buy(my_store, my_hero, my_map)
        elif user_input.lower() in movement_options['character']:
            my_map.display()
            my_hero.display()
        elif user_input.lower() in movement_options['win']:
            auto_win(my_settings, my_map)
        elif user_input.lower() in movement_options['look']:
            print("\nYou survey your surroundings to "
                  "check terrain and obstacles...")
            my_map.display()
            look_around(my_map, my_hero.check_energy(), my_hero.check_money(),
                        my_hero.has_binoculars(), x, y, my_hero)


def update_jewels(my_hero, total_jewels):
    my_hero.add_jewel()
    if my_hero.check_jewels() == total_jewels:
        f = Figlet(font='slant')
        print("YOU GOT ALL THE JEWELS!!")
        print(f.renderText('You Win!'))
        quit()
    print(f"You need to find "
          f"{total_jewels - my_hero.check_jewels()} more magic jewels.")


def play_square(total_jewels, my_hero, sq_details):
    if sq_details['power_bar']:
        print(f"You found a power bar. You gain 2 energy!")
        my_hero.add_energy(2)
    energy = total_energy_cost(my_hero, sq_details)
    if sq_details['terrain'] != 'lake':
        my_hero.sub_energy(energy)
        print(f"Your hero exhausted {energy} energy "
              f"traveling to the {sq_details['terrain']}")
    else:
        print("You lose no energy using the boat to cross "
              " the lake.")
    if sq_details['jewel']:
        print("You found a magic jewel!")
        update_jewels(my_hero, total_jewels)
    if int(sq_details['money']) > 0:
        print(f"You found a bag of money containing ${sq_details['money']}!")
        my_hero.add_money(sq_details['money'])


def calculate_obs_energy(my_hero, sq_details):
    obs_energy_cost = int(sq_details['obstacle'].get('energy_cost', '0'))
    associated_tool = sq_details['obstacle'].get('associated_tools', [])
    tool = my_hero.get_tool(
        associated_tool[0].lower().replace(' ', '_')
        if associated_tool else '')
    if tool:
        # hero has the tool
        obs_energy_saved = int(tool.get('energy_saved', '0'))
        print(f"You use the "
              f"{tool['name']} on the {sq_details['obstacle']['name']}. "
              f"You save {obs_energy_saved} energy.")
        return obs_energy_cost - obs_energy_saved
    print(f"You don't have the "
          f"{associated_tool[0] if associated_tool else ''} to get over "
          f"the {sq_details['obstacle'].get('name', 'Could not get name')}.")
    return obs_energy_cost


def total_energy_cost(my_hero, sq_details):
    # Get the terrain energy cost
    if sq_details['terrain'] == 'plain':
        terrain_energy_cost = 1
    else:
        terrain_energy_cost = 2
    # Get the obstacle energy cost
    if sq_details['obstacle']:
        return terrain_energy_cost + calculate_obs_energy(my_hero, sq_details)
    # If no obstacle
    return terrain_energy_cost


def check_move(my_hero, sq_details, my_map, my_store, power_bar_cost):
    if my_hero.check_energy() >= total_energy_cost(my_hero, sq_details):
        if sq_details['terrain'] == 'lake':
            if not my_hero.has_boat():
                my_map.display()
                print("You do not have a boat and "
                      "are unable to cross the lake.")
                print("You lose one energy "
                      "because you forgot you don't know how to swim.")
                my_hero.sub_energy(1)
                my_hero.short_display()
                return False
        return True
    else:
        # not enough energy, get map to display the original map before moving
        # use temps so we get the user's intended square's details to display
        my_map.display()
        terrain = sq_details['terrain']
        print(f"You tried but failed to make it to the {terrain}.")
        print(f"The {terrain} cost more energy than you have.  "
              f"Your hero only has {my_hero.check_energy()} energy.")
        if my_hero.check_money() < power_bar_cost and my_hero.check_energy() <= 5:
            x, y = my_hero.get_location()
            check_low_energy(my_map, my_hero.check_energy(), x, y, my_hero)
        while True:
            print("\n1. Open Store")
            print("2. Pick a different move direction.")
            reply = input("What would you like to do?: ")
            if reply == '1':
                buy(my_store, my_hero, my_map)
            return False


def auto_win(settings, my_map):
    f = Figlet(font='slant')
    my_map.reveal_all_terrain()
    my_map.display()
    print(f'You found all {settings.jewels} jewels!')
    print(f.renderText('You Win!'))
    exit()


def buy(my_store, my_hero, my_map):
    my_hero.short_display()
    my_store.display()
    tool = my_store.prompt_for_tool_selection()
    my_map.display()
    item = my_store.check_tool(tool)

    if item['name'] == 'power bar':
        purchased = my_hero.buy_power_bar(item)
        if purchased:
            print(f"You purchased the power bar. "
                  f"You gain {item['energy_saved']} energy.")
            my_hero.short_display()
            return
        else:
            print(f"Not enough money to buy the power bar. Leaving store.")
            return
    else:
        purchased = my_hero.buy_tool(item)
        if purchased:
            if item['name'] == 'binoculars':
                x, y = my_hero.get_location()
                my_map.move_into_square(x, y, my_hero.has_binoculars())
                my_map.display()
            print(f"You purchased the {item['name']}.")
            my_hero.display()
            my_store.return_selected_tool(tool)
            return
        else:
            print(f"No purchase made. Not enough money "
                  f"to buy the {item['name']}. Leaving store.")
            return

    # Code for the old store, keep around until we know what we want to do.
    # purchased = ''
    # while not purchased:
    #     item = my_store.buy_tool()
    #     # if it's a power bar, add it through the buy_power_bar so it doesn't get added to tool list
    #     if item[0] == 'power_bar':
    #         purchased = my_hero.buy_power_bar(item[1])
    #         if purchased:
    #             print(f"You purchased the power bar. You gain {item[1]['energy_saved']} energy.")
    #             my_hero.short_display()
    #             return
    #     purchased = my_hero.buy_tool(item[1])
    # if purchased is True:
    #     print(f"You purchased the {item[1]['name']}.")
    #     my_hero.display()
    # else:
    #     print('No purchase made. Returning to main menu.')
    # return


def look_around(my_map, energy, money, binoculars, x, y, my_hero):
    try:
        n_details = my_map.check_square(x, y + 1)
        if total_energy_cost(my_hero, n_details) >= energy:
            n_move = False
        else:
            n_move = True
        print(f"One Move North: "
              f"{n_details['terrain'].title()}: "
              f"{n_details['obstacle'].get('name', 'No Obstacle').title()}")
    except InvalidMapSquare:
        n_move = False
    try:
        s_details = my_map.check_square(x, y - 1)
        if total_energy_cost(my_hero, s_details) >= energy:
            s_move = False
        else:
            s_move = True
        print(f"One Move South: "
              f"{s_details['terrain'].title()}: "
              f"{s_details['obstacle'].get('name', 'No Obstacle').title()}")
    except InvalidMapSquare:
        s_move = False

    try:
        e_details = my_map.check_square(x + 1, y)
        if total_energy_cost(my_hero, e_details) >= energy:
            e_move = False
        else:
            e_move = True
        print(f"One Move East: "
              f"{e_details['terrain'].title()}: "
              f"{e_details['obstacle'].get('name', 'No Obstacle').title()}")
    except InvalidMapSquare:
        e_move = False

    try:
        w_details = my_map.check_square(x - 1, y)
        if total_energy_cost(my_hero, w_details) >= energy:
            w_move = False
        else:
            w_move = True
        print(f"One Move West: "
              f"{w_details['terrain'].title()}: "
              f"{w_details['obstacle'].get('name', 'No Obstacle').title()}")
    except InvalidMapSquare:
        w_move = False
    if binoculars:
        try:
            nn_details = my_map.check_square(x, y + 2)
            print(f"Two Moves North: "
                  f"{nn_details['terrain'].title()}: "
                  f"{nn_details['obstacle'].get('name', 'No Obstacle').title()}")
        except InvalidMapSquare:
            pass
        try:
            ss_details = my_map.check_square(x, y - 2)
            print(f"Two Moves South: "
                  f"{ss_details['terrain'].title()}: "
                  f"{ss_details['obstacle'].get('name', 'No Obstacle').title()}")
        except InvalidMapSquare:
            pass
        try:
            ee_details = my_map.check_square(x + 2, y)
            print(f"Two Moves East: "
                  f"{ee_details['terrain'].title()}: "
                  f"{ee_details['obstacle'].get('name', 'No Obstacle').title()}")
        except InvalidMapSquare:
            pass
        try:
            ww_details = my_map.check_square(x - 2, y)
            print(f"Two Moves West: "
                  f"{ww_details['terrain'].title()}: "
                  f"{ww_details['obstacle'].get('name', 'No Obstacle').title()}")
        except InvalidMapSquare:
            pass

    if not any([n_move, s_move, e_move, w_move]) and money < 5:
        f = Figlet(font='slant')
        print("You ran out of energy to move anywhere and you don't "
              "have enough money to buy energy.")
        print(f.renderText('You Lose'))
        quit()


def check_low_energy(my_map, energy, x, y, my_hero):
    try:
        n_details = my_map.check_square(x, y + 1)
        if total_energy_cost(my_hero, n_details) >= energy:
            n_move = False
        else:
            n_move = True
    except InvalidMapSquare:
        n_move = False
    try:
        s_details = my_map.check_square(x, y - 1)
        if total_energy_cost(my_hero, s_details) >= energy:
            s_move = False
        else:
            s_move = True
    except InvalidMapSquare:
        s_move = False

    try:
        e_details = my_map.check_square(x + 1, y)
        if total_energy_cost(my_hero, e_details) >= energy:
            e_move = False
        else:
            e_move = True
    except InvalidMapSquare:
        e_move = False

    try:
        w_details = my_map.check_square(x - 1, y)
        if total_energy_cost(my_hero, w_details) >= energy:
            w_move = False
        else:
            w_move = True
    except InvalidMapSquare:
        w_move = False

    if not any([n_move, s_move, e_move, w_move]):
        f = Figlet(font='slant')
        my_hero.display()
        print("You ran out of energy to move anywhere and you don't "
              "have enough money to buy energy.")
        print(f.renderText('You Lose!'))
        quit()


def print_instructions():
    print("\nEnter L to look around and view obstacles and terrain type.")
    print("Enter W A S D to move north, west, south, east.")
    print("Enter C to view your character information.")
    print("Enter Store to open the store.")
    print("Enter `win` to finish the game.")
    print("Enter Q to quit the game.")
    return input("Enter your selection: ")


main()
