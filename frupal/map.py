# Map related info
import random
import colorama

from util import cls
from colorama import Fore, Style


class InvalidMapSquare(Exception):
    # Custom exception when the Map Class calls an invalid square
    pass


class Map:
    def __init__(self, size=None, jewel_qty=None, obstacle_qty=None,
                 obstacle_dict=None, power_bar_qty=None,
                 squares_with_money=None):

        # List of all parameters to check
        param_list = [size, jewel_qty, obstacle_qty,
                      power_bar_qty, squares_with_money]

        if not all(isinstance(x, int) for x in param_list):
            raise TypeError(f"Map Object Can Only Be Initialized with "
                            f"Integers.  Tried to initialize "
                            f"with {param_list}")
        if not size or size < 2:
            raise ValueError("Size must exist and be greater than one")
        if any(x > (size * size) for x in param_list):
            raise ValueError(
                "Number of objects to set must "
                "be less than the total number of squares")

        if any(x < 0 for x in param_list):
            raise ValueError(
                "Parameters must be greater than or equal to zero")

        self.__size__ = size
        self.__jewel_qty__ = jewel_qty
        self.__power_bar_qty__ = power_bar_qty
        self.__obstacle_qty__ = obstacle_qty
        self.__obstacle_dict__ = obstacle_dict
        self.__squares_with_money__ = squares_with_money
        self.__map__ = {}

        # Default Position of the hero is 1,1
        self.__hero_position__ = {'x': 1, 'y': 1}

        # To make sure the number of lakes leaves enough
        # room on the map for the obstacle_qty
        num_lakes = 0

        # The map dictionary is built backwards
        for y in range(1, self.__size__ + 1):
            self.__map__[y] = {}
            for x in range(1, self.__size__ + 1):
                self.__map__[y][x] = MapSquare()
                if self.__map__[y][x].is_lake():
                    num_lakes += 1

        # Adjust obstacle_qty if there are too many lakes
        remaining_squares = (self.__size__*self.__size__) - num_lakes
        if remaining_squares <= self.__obstacle_qty__:
            self.__obstacle_qty__ = int(remaining_squares/2)

        self.__add_jewels_to_map__()
        self.__add_obstacles_to_map__()
        self.__add_power_bars_to_map__()
        self.__add_money_to_map__()
        colorama.init()
        self.__map_star__ = '*'
        self.__formatted_map_star__ = f'{Fore.YELLOW}{self.__map_star__}'

        # Checks to see if a jewel was on the initial
        # square and put it back if there was one
        initial_square = self.__map__[
                self.__hero_position__['y']][self.__hero_position__['x']]

        if initial_square.get_square_details()['jewel']:
            self.move_into_square(self.__hero_position__['x'],
                                  self.__hero_position__['y'])
            initial_square.__magic_jewel__ = True

        else:
            self.move_into_square(self.__hero_position__['x'],
                                  self.__hero_position__['y'])

    def __repr__(self):
        return f"A {self.__size__} by {self.__size__} Map"

    def display(self):
        cls()
        self.__display_long_row_of_stars__()
        for col in reversed(sorted(self.__map__.keys())):
            row = self.__map__[col]
            self.__display_column_row_with_details__(row)
            self.__display_long_row_of_stars__()
        print(Style.RESET_ALL)

    def __display_long_row_of_stars__(self):
        number_of_stars = 5 * self.__size__ - (self.__size__ - 1)
        print(Fore.YELLOW + (number_of_stars * f'{self.__map_star__}'))

    def __display_column_row_with_details__(self, col_row):
        output = f'{self.__formatted_map_star__}'
        for col in col_row.values():
            output += f' {col.__terrain_initial__} ' \
                f'{self.__formatted_map_star__}'
        print(output)

    def check_square(self, x=None, y=None):
        if None in (x, y):
            raise InvalidMapSquare('Both "x" and "y" values must be set')
        coordinates = [x, y]
        if any(i > self.__size__ for i in coordinates):
            raise InvalidMapSquare(
                f"The values {x}, {y} are invalid coordinates.  "
                f"Max size is {self.__size__}x{self.__size__}.")
        if any(i < 1 for i in coordinates):
            raise InvalidMapSquare(f"The values {x}, {y} are invalid "
                                   f"coordinates.  Max size is 1x1.")

        map_sq = self.__map__[y][x]
        details = map_sq.get_square_details()
        return details

    def move_into_square(self, x=None, y=None, binoculars=False):
        if None in (x, y):
            raise InvalidMapSquare('Both "x" and "y" values must be set')
        if binoculars not in [True, False]:
            raise TypeError("'binoculars' must be a boolean value")

        coordinates = [x, y]

        if any(i > self.__size__ for i in coordinates):
            raise InvalidMapSquare(
                f"The values {x}, {y} are invalid coordinates.  "
                f"Max size is {self.__size__}x{self.__size__}.")
        if any(i < 1 for i in coordinates):
            raise InvalidMapSquare(f"The values {x}, {y} are invalid "
                                   f"coordinates.  Max size is 1x1.")

        self.__hero_position__ = {'x': x, 'y': y}
        neighbors = self.__hero_neighbors__(binoculars)
        map_sq = self.__map__[y][x]
        details = map_sq.get_square_details()
        map_sq.mark_visited()

        # This updates the icon on the square from ? to a letter
        for y_coord in range(1, self.__size__ + 1):
            for x_coord in range(1, self.__size__ + 1):
                if x_coord == x and y_coord == y:
                    self.__map__[y_coord][x_coord].update_square_display(
                        hero_on_square=True)
                elif (x_coord, y_coord) in neighbors:
                    self.__map__[y_coord][x_coord].mark_viewed()
                    self.__map__[y_coord][x_coord].update_square_display()
                else:
                    self.__map__[y_coord][x_coord].update_square_display()

        return details

    def reveal_all_terrain(self):
        for y_coord in range(1, self.__size__ + 1):
            for x_coord in range(1, self.__size__ + 1):
                self.__map__[y_coord][x_coord].mark_viewed()
                self.__map__[y_coord][x_coord].update_square_display()

    def __hero_neighbors__(self, binoculars=False):
        # Returns all possible neighbors, even invalid ones
        if not isinstance(binoculars, bool):
            raise ValueError(f'Invalid data type passed to '
                             f'"update_square_display" method.'
                             f'  Must be a boolean.  "binoculars" '
                             f'instead was {binoculars}')
        x = self.__hero_position__['x']
        y = self.__hero_position__['y']
        to_return = []
        x_neigh = range(x-2 if binoculars else x-1, x+3 if binoculars else x+2)
        y_neigh = range(y-2 if binoculars else y-1, y+3 if binoculars else y+2)
        for x1 in x_neigh:
            for y1 in y_neigh:
                to_return.append((x1, y1))

        return set(to_return)

    def __add_jewels_to_map__(self):
        jewels_added = 0
        while jewels_added is not self.__jewel_qty__:
            rand_x = random.randint(1, self.__size__)
            rand_y = random.randint(1, self.__size__)

            details = self.__map__[rand_y][rand_x].get_square_details()

            if not details['jewel']:
                self.__map__[rand_y][rand_x].set_magic_jewel()
                jewels_added += 1

    def __add_power_bars_to_map__(self):
        power_bars_added = 0
        while power_bars_added is not self.__power_bar_qty__:
            rand_x = random.randint(1, self.__size__)
            rand_y = random.randint(1, self.__size__)

            details = self.__map__[rand_y][rand_x].get_square_details()

            if not details['power_bar']:
                self.__map__[rand_y][rand_x].set_power_bar()
                power_bars_added += 1

    def __add_money_to_map__(self):
        money_added = 0
        while money_added is not self.__squares_with_money__:
            rand_x = random.randint(1, self.__size__)
            rand_y = random.randint(1, self.__size__)

            details = self.__map__[rand_y][rand_x].get_square_details()

            if not details['money']:
                self.__map__[rand_y][rand_x].set_money()
                money_added += 1

    def __add_obstacles_to_map__(self):
        obstacles_added = 0
        while obstacles_added is not self.__obstacle_qty__:
            rand_x = random.randint(1, self.__size__)
            rand_y = random.randint(1, self.__size__)

            details = self.__map__[rand_y][rand_x].get_square_details()

            if not details['obstacle'] and details['terrain'] != "lake":
                key = random.choice(list(self.__obstacle_dict__ .keys()))
                self.__map__[rand_y][rand_x].\
                    set_obstacle(self.__obstacle_dict__[key])
                obstacles_added += 1


class MapSquare:
    # One in Four chance of getting a magic jewel
    terrain_types = {
        'plain': f'{Fore.LIGHTYELLOW_EX}P',
        'forest': f'{Fore.LIGHTGREEN_EX}F',
        'mountain': f'{Fore.LIGHTMAGENTA_EX}M',
        'bog': f'{Fore.LIGHTWHITE_EX}B',
        'lake': f'{Fore.LIGHTBLUE_EX}L'
    }

    def __init__(self):
        self.__terrain__ = random.choice(list(self.terrain_types.keys()))
        self.__magic_jewel__ = False
        self.__money__ = 0
        self.__visited__ = False
        self.__viewed__ = False
        self.__obstacle__ = {}
        self.__unknown_terrain__ = f'{Fore.RED}?'
        self.__terrain_initial__ = self.__unknown_terrain__
        self.__power_bar__ = False

    def __repr__(self):
        return f'{self.__terrain__} {self.__magic_jewel__} ' \
            f'{self.__money__} {self.__visited__}, ' \
            f'{self.__obstacle__} {self.__power_bar__} {id(self)}'

    def update_square_display(self, hero_on_square=False):
        if not isinstance(hero_on_square, bool):
            raise ValueError(f'Invalid data type passed to '
                             f'"update_square_display" method.'
                             f'  Must be a boolean instead '
                             f'"hero_on_square" '
                             f'was {hero_on_square}')

        if hero_on_square:
            self.__terrain_initial__ = f'{Fore.LIGHTCYAN_EX}&'
        elif self.__visited__ or self.__viewed__:
            self.__terrain_initial__ = self.terrain_types[self.__terrain__]
        else:
            self.__terrain_initial__ = self.__unknown_terrain__

    def get_square_details(self):
        data = {
            'terrain': self.__terrain__,
            'jewel': self.__magic_jewel__,
            'money': self.__money__,
            'visited': self.__visited__,
            'obstacle': self.__obstacle__,
            'power_bar': self.__power_bar__
        }
        return data

    def mark_visited(self):
        # If you visit a square, you get the magic jewel
        self.__visited__ = True
        self.__magic_jewel__ = False
        self.__power_bar__ = False
        self.__money__ = 0

    def mark_viewed(self):
        # If you view the square, you remember it
        self.__viewed__ = True

    def set_magic_jewel(self):
        self.__magic_jewel__ = True

    def set_obstacle(self, obstacle):
        self.__obstacle__ = obstacle

    def set_power_bar(self):
        self.__power_bar__ = True

    def set_money(self):
        self.__money__ = random.randint(1, 10)

    def is_lake(self):
        return True if self.__terrain__ == 'lake' else False
