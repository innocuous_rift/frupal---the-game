from pyfiglet import Figlet


class Store:
    def __init__(self, tools):
        self.tools = tools
        self.store_list = []
        self.update_store_list()

    def display(self):
        f = Figlet(font='slant')
        print(f.renderText('Store'))
        print('{:<10}{:<20}{:<20}'.format('Position', 'Name', 'Price'))
        print('-' * 40)
        for pos, (key, name, price) in enumerate(self.store_list):
            print('{:<10}{:<20}{:<20}'.format(pos+1, name, price))

    def check_tool(self, tool_pos):
        k, n, p = self.store_list[tool_pos - 1]
        return self.tools.get(k)

    def return_selected_tool(self, tool_pos):
        k, n, p = self.store_list[tool_pos - 1]
        # tool_dict = self.tools.get(k)
        # return k, tool_dict

    def update_store_list(self):
        self.store_list = [(k, v['name'], v['price'])
                           for k, v in self.tools.items()]

    def prompt_for_tool_selection(self):
        tool_pos = -1
        while tool_pos < 1 or tool_pos > len(self.store_list):
            try:
                tool_pos = input(
                    f'Select an item to buy.  Enter a number '
                    f'between 1 and {len(self.store_list)}: ')
                tool_pos = int(tool_pos)
            except ValueError:
                tool_pos = -1
                print(f'Invalid input!')

        return tool_pos

    def buy_tool(self):
        self.display()
        tool = self.prompt_for_tool_selection()
        print(f'the number selected was: {tool}')
        return self.return_selected_tool(tool)
