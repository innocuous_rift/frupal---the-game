# Useful functions other programs may want to use
import os


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')
