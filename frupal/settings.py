import yaml
from pyfiglet import Figlet


class Settings:
    def __init__(self):

        self.map_size = 0
        self.difficulty = "easy"
        self.money = 0
        self.energy = 0
        self.obstacles = {}
        self.tools = {}
        self.jewels = 0
        self.density = 0
        self.money_squares = 0
        self.powerbar_squares = 0

        try:
            with open("frupal/settings.yaml", 'r') as stream:
                self.sload = yaml.safe_load(stream)

            self.map_size = self.sload['map']['map_size']
            self.difficulty = self.sload['map']['difficulty']
            self.money = self.sload['hero']['money']
            self.energy = self.sload['hero']['energy']
            self.obstacles = self.sload['obstacles']
            self.tools = self.sload['tools']
            self.jewels = self.calculate_jewels(self.difficulty)

            self.density = self.calculate_density(
                self.map_size, self.difficulty)
            self.money_squares = self.sload['map']['money_squares']
            self.powerbar_squares = self.sload['map']['powerbars']

        except FileNotFoundError:
            print('Error, file was not loaded')
            pass

    def print_settings(self):
        f = Figlet(font='slant')
        print(f.renderText('Settings'))
        print("Here are the current settings:\n")
        print("Map Size: ", self.map_size)
        print("Difficulty: ", self.difficulty)
        print("Starting Money: ", self.money)
        print("Starting Energy: ", self.energy)

        tool_list = [x['name'] for x in self.tools.values()]
        print_comma_list("Tools: ", tool_list)
        obstacle_list = [x['name'] for x in self.obstacles.values()]
        print_comma_list("Obstacles: ", obstacle_list)

    def settings_menu(self):
        self.print_settings()
        while True:
            print("\nSETTINGS MENU")
            print("1. Add/remove tools")
            print("2. Add/remove obstacles")
            print("3. Set map size")
            print("4. Set difficulty")
            print("5. Set starting money")
            print("6. Set starting energy")
            print("7. Back to Main Menu\n")

            reply = input("What would you like to do?: ")

            if reply == "1":
                print("Settings: Tools")
                self.tool_settings()
            elif reply == "2":
                print("Settings: Obstacles")
                self.obstacle_settings()
            elif reply == "3":
                print("Settings: Map Size")
                self.set_map_size()
            elif reply == "4":
                print("Settings: Difficulty")
                self.set_difficulty()
            elif reply == "5":
                print("Settings: Starting Money")
                self.set_money()
            elif reply == "6":
                print("Settings: Starting Energy")
                self.set_energy()
            elif reply == "7":
                self.jewels = self.calculate_jewels(self.difficulty)
                self.density = self.calculate_density(
                    self.map_size, self.difficulty)

                return
            else:
                print("Not a valid choice. Try again: ")

    # Function name:    tool_settings
    # Description:      runs through tool settings menu
    # Parameters:       none
    # Return:           none
    def tool_settings(self):
        print("\nADD/REMOVE TOOLS")
        print("1. Add tool")
        print("2. Remove tool")
        print("3. Update tool\n")

        reply = input("What would you like to do?: ")
        if reply == "1":
            self.add_tool()
        if reply == "2":
            self.remove_tool()
        if reply == "3":
            self.update_tool()

    # Function name:    add_tool
    # Description:      adds a tool to the Setting's dictionary
    #                   of tools via user input
    # Parameters:       none
    # Return:           none
    def add_tool(self):

        tool_list = [x['name'] for x in self.tools.values()]
        print_comma_list("\nCURRENT TOOL LIST: ", tool_list)

        loop = True

        name = input("\nPlease enter tool name: ")

        for k, v in self.tools.items():
            if name.lower() in v['name'].lower():
                answer = input("\nError: That tool already exists! "
                               "Would you like to update it? ")
                if answer == 'y' or answer == 'Y':
                    self.update_tool()
                return

        while loop:
            price = input("Please enter tool price: ")
            try:
                p = float(price)
                if p >= 0.00:
                    loop = False
                else:
                    print_float_error("price")
            except ValueError:
                print_float_error("price")

        loop = True

        while loop:
            energy_saved = input("Please enter the amount "
                                 "of the energy the tool saves: ")
            try:
                energy = int(energy_saved)
                if energy > 0:
                    loop = False
                else:
                    print_int_error("energy saved")
            except ValueError:
                print_int_error("energy saved")

        temp = self.tools[name.lower().replace(' ', '_')] = {
            'name': name,
            'price': float(price),
            'energy_saved': int(energy_saved)
        }

        print("\nNEW TOOL: ")
        for i in temp:
            print(i, ": ", temp[i])

        return

    # Function name:    remove_tool
    # Description:      removes a tool from the Setting's
    #                   dictionary of tools based on user input
    # Parameters:       none
    # Return:
    def remove_tool(self):
        tool_list = [x['name'] for x in self.tools.values()]
        print_comma_list("\nCURRENT TOOL LIST: ", tool_list)

        tool_2_remove = input("\nPlease enter the name of the "
                              "tool you would like to remove: ")
        things_2_pop = []

        for k, v in self.tools.items():
            if tool_2_remove.lower() in v['name'].lower():
                things_2_pop.append(k)

        if not things_2_pop:
            print("\n********")
            print("Error: Tool could not be found!")
            print("********\n")

        for item in things_2_pop:
            self.tools.pop(item)

        tool_list = [x['name'] for x in self.tools.values()]
        print_comma_list("\nUPDATED TOOL LIST: ", tool_list)

    # Function name:    update_tool
    # Description:      updates a tool from the Setting's
    #                   dictionary of tools based on
    #                   user input
    # Parameters:       none
    # Return:           none
    def update_tool(self):

        # while the user still wants to update tools
        update = True
        while update:
            tool_list = [x['name'] for x in self.tools.values()]
            print_comma_list("\nTOOL LIST: ", tool_list)

            tool_2_update = input("\nPlease enter the name of the "
                                  "tool you would like to update: ")
            found = False

            for k, v in self.tools.items():
                if tool_2_update.lower() in v['name'].lower():
                    found = True
                    valid = False
                    num_fields = len(v)
                    while not valid:
                        valid = self.update_tool_helper(v, num_fields)
                        if valid:
                            v = valid
                            self.print_tool_settings("\nUPDATED SETTINGS: ", v)
                            update_again = input("\nWould you like to "
                                                 "update another tool "
                                                 "(y or n)? ")
                            if update_again != 'y' and \
                                    update_again != 'Y':
                                update = False
            if not found:
                add = input("\nError: That tool is not in the list! "
                            "Would you like to add it (y or n)? ")
                if add == 'y' or add == 'Y':
                    self.add_tool()
                    update = False
                else:
                    try_again = input("\nWould you like to update a "
                                      "different tool (y or n)? ")
                    if try_again != 'y' and try_again != 'Y':
                        update = False

    # Function name: update_tool_helper
    # Description:   updates a specified tool given the dictionary
    #                and the number of fields (this is a helper
    #                for the update_tool function)
    # Parameters:    a tool_2_update (a dictionary)
    #                num_fields
    # Return:        the updated tool or False
    def update_tool_helper(self, tool_2_update, num_fields):

        self.print_tool_settings("\nCURRENT SETTINGS", tool_2_update)
        print("\nWhich field would you "
              "like to update? (1 -", num_fields, end="")
        key_2_update = input(")? ")

        try:
            index = int(key_2_update) - 1
            if index < 0 or index > num_fields - 1:
                print_invalid_field_error(num_fields)
            else:
                new_value = input("\nPlease enter "
                                  "the new value: ")
                key_list = list(tool_2_update.keys())
                if ((key_list[index] != "energy_saved" and
                     key_list[index] != "price") or
                        ((key_list[index] == "energy_saved" or
                          key_list[index] == "price") and
                         (isint(new_value) or
                          isfloat(new_value)))):
                    if isint(new_value):
                        if int(new_value) < 1:
                            print_int_error(key_list[index])
                            return False
                        tool_2_update[key_list[index]] = int(new_value)
                    elif isfloat(new_value):
                        if float(new_value) < 0:
                            print_float_error(key_list[index])
                            return False
                        tool_2_update[key_list[index]] = float(new_value)
                    else:
                        tool_2_update[key_list[index]] = new_value
                    return tool_2_update
                else:
                    print("\n********")
                    print("Error: That is not a valid"
                          " value for that field."
                          " Please enter either"
                          " integer or decimal value.")
                    print("********\n")
                    return False
        except ValueError:
            print_invalid_field_error(num_fields)
            return False

    # Function name:    print_tool_settings
    # Description:      prints the settings of a given tool in
    #                   numbered list format
    # Parameters:       identifier of the list (a string)
    #                   the tool
    # Return:           none
    def print_tool_settings(self, identifier, tool):
        i = 1
        print(identifier)
        for item in tool:
            print(i, ". ", item, ": ", tool[item])
            i += 1

    # Function name:    obstacle_settings
    # Description:      runs through obstacle settings menu
    # Parameters:       none
    # Return:           none

    def obstacle_settings(self):
        print("\nADD/REMOVE OBSTACLES")
        print("1. Add obstacle")
        print("2. Remove obstacle")
        print("3. Update obstacle\n")

        reply = input("What would you like to do?: ")
        if reply == "1":
            self.add_obstacle()
        if reply == "2":
            self.remove_obstacle()
        if reply == "3":
            self.update_obstacle()

    # Function name:    add_obstacle
    # Description:      adds an obstacle to the Setting's dictionary
    #                   of obstacles via user input
    # Parameters:       none
    # Return:           none

    def add_obstacle(self):

        obstacle_list = [x['name'] for x in self.obstacles.values()]
        print_comma_list("\nCURRENT OBSTACLE LIST: ", obstacle_list)

        loop = True

        name = input("\nPlease enter obstacle name: ")

        for k, v in self.obstacles.items():
            if name.lower() in v['name'].lower():
                answer = input("\nError: That obstacle already exists! "
                               "Would you like to update it? ")
                if answer == 'y' or answer == 'Y':
                    self.update_obstacle()
                return

        while loop:
            energy_cost = input("Please enter the amount "
                                "of energy the obstacle"
                                " requires to overcome it: ")
            try:
                energy = int(energy_cost)
                if energy > 0:
                    loop = False
                else:
                    print_int_error("energy_cost")
            except ValueError:
                print_int_error("energy_cost")

        loop = True
        tools = []

        while loop:
            tool_2_add = (input("Please enter the name of the tool "
                                "the player requires to overcome "
                                "this obstacle: "))
            tools.append(tool_2_add.lower())
            answer = input("\nAre there any other tools associated with"
                           " this obstacle (y or n)? ")
            if answer != 'y' and answer != 'Y':
                loop = False

        temp = self.obstacles[name.lower().replace(' ', '_')] = {
            'name': name,
            'energy_cost': int(energy_cost),
            'associated_tools': tools
        }

        print("\nNEW OBSTACLE: ")
        for i in temp:
            if i == 'associated_tools':
                print_comma_list("associated_tools: ", temp[i])
            else:
                print(i, ": ", temp[i])

        return

    # Function name:    remove_obstacle
    # Description:      removes an obstacle from the Setting's
    #                   dictionary of obstacles based on user input
    # Parameters:       none
    # Return:           none

    def remove_obstacle(self):

        obstacle_list = [x['name'] for x in self.obstacles.values()]
        print_comma_list("\nCURRENT OBSTACLE LIST: ", obstacle_list)

        obstacle_2_remove = input("\nPlease enter the name of the "
                                  "obstacle you would like to remove: ")
        things_2_pop = []

        for k, v in self.obstacles.items():
            if obstacle_2_remove.lower() in v['name'].lower():
                things_2_pop.append(k)

        if things_2_pop == []:
            print("\n********")
            print("Error: Obstacle could not be deleted!")
            print("********\n")

        for item in things_2_pop:
            self.obstacles.pop(item)

        obstacle_list = [x['name'] for x in self.obstacles.values()]
        print_comma_list("\nUPDATED OBSTACLE LIST: ", obstacle_list)

    # Function name:    update_obstacle
    # Description:      updates an obstacle from the Setting's
    #                   dictionary of obstacles based on
    #                   user input
    # Parameters:       none
    # Return:           none

    def update_obstacle(self):

        # while the user still wants to update obstacles
        update = True
        while update:
            obstacle_list = [x['name'] for x in self.obstacles.values()]
            print_comma_list("\nOBSTACLE LIST: ", obstacle_list)

            obstacle_2_update = input("\nPlease enter the name of the "
                                      "obstacle you would like to update: ")
            found = False

            for k, v in self.obstacles.items():
                if obstacle_2_update.lower() in v['name'].lower():
                    found = True
                    valid = False
                    num_fields = len(v)
                    while not valid:
                        valid = self.update_obstacle_helper(v, num_fields)
                        if valid:
                            v = valid
                            self.print_obstacle_settings("\nUPDATED "
                                                         "SETTINGS: ", v)
                            update_again = input("\nWould you like to "
                                                 "update another "
                                                 "obstacle (y or n)? ")
                            if update_again != 'y' and update_again != 'Y':
                                update = False
            if not found:
                add = input("\nError: That obstacle is not in the list! "
                            "Would you like to add it (y or n)? ")
                if add == 'y' or add == 'Y':
                    self.add_obstacle()
                    update = False
                else:
                    try_again = input("\nWould you like to update a "
                                      "different obstacle (y or n)? ")
                    if try_again != 'y' and try_again != 'Y':
                        update = False

    # Function name:    update_obstacle_helper
    # Description:
    # Parameters:
    # Return:           the updated obstacle or False
    def update_obstacle_helper(self, o, num_fields):

        self.print_obstacle_settings("CURRENT SETTINGS: ", o)
        print("\nWhich field would you "
              "like to update? (1 -", num_fields, end="")
        key_2_update = input(")? ")
        try:
            index = int(key_2_update) - 1
            if index < 0 or index > num_fields - 1:
                print_invalid_field_error(num_fields)
            else:
                key_list = list(o.keys())
                if key_list[index] == "associated_tools":
                    new_obstacle = self.update_associated_tools(o)
                    o = new_obstacle
                    return o
                else:
                    new_value = input("\nPlease enter "
                                      "the new value: ")
                    if key_list[index] != "energy_cost" \
                            or (key_list[index] == "energy_cost"
                                and isint(new_value)):
                        if isint(new_value):
                            if int(new_value) < 0:
                                print_int_error(key_list[index])
                                return False
                            else:
                                o[key_list[index]] = int(new_value)
                        else:
                            o[key_list[index]] = new_value
                        return o
                    else:
                        print_int_error(key_list[index])
                        return False
        except ValueError:
            print_invalid_field_error(num_fields)
            return False

    # Function name:
    # Description:
    # Parameters:
    # Return:
    def update_associated_tools(self, o):
        print_comma_list("\nCURRENT TOOLS"
                         " ASSOCIATED WITH "
                         "OBSTACLE: ", o["associated_tools"])
        valid = False
        while not valid:
            num_tools = len(o["associated_tools"])
            if num_tools > 1:

                print("\nUPDATE ASSOCIATED TOOLS")
                print("1. Add associated tool")
                print("2. Remove associated tool")
                print("3. Update associated tool\n")

                reply = input("What would you like to do (1 - 3)?: ")
                if reply == "1":
                    print("ADD ASSOCIATED TOOL\n")
                    user_input = input("Please enter the name of the "
                                       "associated tool you would like "
                                       "to add: ")
                    o["associated_tools"].append(user_input)
                    valid = True
                elif reply == "2":
                    print("REMOVE ASSOCIATED TOOL\n")
                    print_num_list("TOOLS: ", o["associated_tools"])
                    print("Which associated tool"
                          " would you like to remove (1 -", num_tools, end="")
                    user_input = input(")? ")
                    try:
                        index = int(user_input) - 1
                        if index < 0 or index > num_tools - 1:
                            print_invalid_field_error(num_tools)
                            valid = False
                        else:
                            o["associated_tools"].pop(index)
                            valid = True
                    except ValueError:
                        print_invalid_field_error(num_tools)
                elif reply == "3":
                    print("UPDATE ASSOCIATED TOOL")
                    print_num_list("TOOLS: ", o["associated_tools"])
                    print("\nWhich associated tool"
                          " would you like to update (1 -", num_tools, end="")
                    user_input = input(")? ")
                    try:
                        index = int(user_input) - 1
                        if index < 0 or index > num_tools - 1:
                            print_invalid_field_error(num_tools)
                        else:
                            new_value = input("\nPlease enter "
                                              "the new value: ")
                            o["associated_tools"][index] = new_value
                            valid = True
                    except ValueError:
                        print_invalid_field_error(num_tools)
                else:
                    print_invalid_field_error("3")

            else:
                print("\nUPDATE ASSOCIATED TOOLS")
                print("1. Add associated tool")
                print("2. Update associated tool\n")

                reply = input("What would you like to do (1 or 2)?: ")
                if reply == "1":
                    print("ADD ASSOCIATED TOOL\n")
                    user_input = input("Please enter the name of the "
                                       "associated tool you would like "
                                       "to add: ")
                    o["associated_tools"].append(user_input)
                    valid = True
                elif reply == "2":
                    print("UPDATE ASSOCIATED TOOL")
                    new_value = input("\nPlease enter "
                                      "the new value: ")
                    o["associated_tools"][0] = new_value
                    valid = True
                else:
                    print_invalid_field_error("2")
            if valid:
                self.print_obstacle_settings("\nUPDATED SETTINGS: ", o)
                update_again = input("\nWould you like to "
                                     "update another "
                                     "associated tool "
                                     "(y or n)? ")
                if update_again != 'y' and \
                        update_again != 'Y':
                    return o
                else:
                    valid = False

    # Function name:    print_obstacle_settings
    # Description:      prints the settings of a given obstacle in
    #                   numbered list format
    # Parameters:       identifier of the obstacle (a string)
    #                   the obstacle
    # Return:           none
    def print_obstacle_settings(self, identifier, obstacle):
        print(identifier)
        i = 1
        for item in obstacle:
            if item == 'associated_tools':
                m = str(i) + " .  " + str(item) + ": "
                print_comma_list(m, obstacle[item])
            else:
                print(i, ". ", item, ": ", obstacle[item])
            i += 1

    def set_map_size(self):
        while True:
            try:
                self.map_size = int(input("Please enter desired "
                                          "map size between 3 and 10: "))
            except ValueError:
                print("Error: That's not a valid map size. Try again.")
                continue
            if self.map_size < 3 or self.map_size > 10:
                print("Map-size must be between 3 and 10. Try again.")
                continue
            return

    def set_money(self):
        while True:
            try:
                self.money = int(input("Please enter "
                                       "desired starting money: "))
            except ValueError:
                print("That's not a valid number. Try again.")
                continue
            if self.money < 0:
                print("Money must be a non-negative number. Try again.")
                continue
            return

    def set_energy(self):
        while True:
            try:
                self.energy = int(input("Please enter "
                                        "desired starting energy: "))
            except ValueError:
                print("That's not a valid number. Try again.")
                continue
            if self.energy < 0:
                print("Energy must be a non-negative number. Try again.")
                continue
            return

    def set_difficulty(self):
        print("1. Easy")
        print("2. Medium")
        print("3. Hard")
        while True:
            try:
                reply = int(input("Please enter desired difficulty (1-3): "))
            except ValueError:
                print("That's not a valid number. Try again.")
                continue
            if reply < 1 or reply > 3:
                print("Not a valid choice. Try again.")
                continue
            if reply == 1:
                self.difficulty = "easy"
            elif reply == 2:
                self.difficulty = "medium"
            elif reply == 3:
                self.difficulty = "hard"
            return

    def calculate_jewels(self, diff):
        if diff == "easy":
            return 3
        elif diff == "medium":
            return 5
        else:
            return 8

    def calculate_density(self, msize, diff):
        if diff == "easy":
            return msize
        elif diff == "medium":
            return msize * 2
        else:
            return msize * msize


# Function name:    isfloat
# Description:      determines whether a string s can be cast as a float
# Parameters:       s (a string)
# Return:           true if it is a float, false otherwise
def isfloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


# Function name:    isint
# Description:      determines whether a string s can be cast as an int
# Parameters:       s (a string)
# Return:           true if it is an int, false otherwise
def isint(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


# Function name:    print_comma_list
# Description:      prints a comma-delimited list given a
#                   string (the list_identifier) and the list itself
# Parameters:       list_identifier (most likely a string)
#                   and a_list (the list to print)
# Return:           none
def print_comma_list(list_identifier, a_list):
    print(list_identifier, end='')
    print(", ".join(a_list))


# Function name:    print_num_list
# Description:      prints a numbered list given a
#                   string (the list_identifier) and the list itself
# Parameters:       list_identifier (most likely a string)
#                   and a_list (the list to print)
# Return:           none
def print_num_list(list_identifier, a_list):
    print(list_identifier)
    for i, item in enumerate(a_list):
        print(f'{i+1}. {item}')


# Function name:    print_float_error
# Description:      prints the appropriate error when the user
#                   should have entered
#                   a float for a given field
# Parameters:       the field the error occurred for
# Return:           none
def print_float_error(field):
    print("\n********")
    print("Error: Please enter the ", field,
          " as a non-negative float - e.g. 5.00. Try again.")
    print("********\n")


# Function name:    print_int_error
# Description:      prints the appropriate error when the user
#                   should have entered
#                   an int for a given field
# Parameters:       the field the error occurred for
# Return:           none
def print_int_error(field):
    print("\n********")
    print("Error: Please enter ", field,
          " as a positive integer value - e.g. 2. Try again.")
    print("********\n")


# Function name:    print_invalid_field_error
# Description:      prints the appropriate error when the user entered either
#                   an invalid value for the field or a field that is
#                   out of range
# Parameters:       the field the error occurred for
# Return:           none
def print_invalid_field_error(maximum):
    print("\n********")
    print("Error: That is not a valid field! "
          "Please enter a value 1 -", maximum, ".")
    print("********")
