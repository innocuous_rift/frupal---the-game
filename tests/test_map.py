import pytest
from pytest import raises
import sys
import os
from frupal.settings import Settings

# Deals with some pathing issues with PyTest and the imports in map.py
myPath = os.path.dirname(os.path.abspath(__file__))

if os.name == 'nt':
    sys.path.insert(0, myPath + '\\..\\frupal')
else:
    sys.path.insert(0, myPath + '/../frupal')

from frupal.map import Map, MapSquare, InvalidMapSquare  # NOQA

terrain_types = ['bog', 'plain', 'forest', 'mountain', 'lake']
my_settings = Settings()
obstacle_dict = my_settings.obstacles


@pytest.fixture()
def setup_small_map():
    yield Map(2, 0, 0, obstacle_dict, 0, 0)


@pytest.fixture()
def setup_medium_map():
    yield Map(3, 0, 0, obstacle_dict, 0, 0)


@pytest.fixture()
def setup_large_map():
    yield Map(10, 2, 2, obstacle_dict, 4, 2)


def test_map_square_created():
    test_map_square = MapSquare()
    assert not test_map_square.__visited__
    assert test_map_square.__terrain__ in terrain_types
    assert test_map_square.__magic_jewel__ is False
    assert isinstance(test_map_square.__money__, int)


def test_map_square_is_visited():
    test_map_square = MapSquare()
    test_map_square.set_money()
    test_map_square.set_magic_jewel()
    assert test_map_square.__visited__ is False
    assert test_map_square.__magic_jewel__ is True
    assert test_map_square.__money__ > 0
    test_map_square.mark_visited()
    assert test_map_square.__visited__ is True
    assert test_map_square.__magic_jewel__ is False
    assert test_map_square.__money__ == 0


def test_map_square_is_viewed():

    test_map_square = MapSquare()
    assert test_map_square.__viewed__ is False
    test_map_square.mark_viewed()
    assert test_map_square.__viewed__ is True


def test_map_square_get_details():
    test_map_square = MapSquare()

    data = test_map_square.get_square_details()
    assert isinstance(data, dict)
    assert test_map_square.__visited__ is False

    test_map_square.mark_visited()
    data = test_map_square.get_square_details()
    assert isinstance(data, dict)
    assert test_map_square.__visited__ is True


def test_map_square_verify_square_disp_invalid_input():
    test_map_square = MapSquare()

    with raises(ValueError) as e:
        test_map_square.update_square_display(
            hero_on_square='Some_String')
    assert str(e.value) == 'Invalid data type passed to ' \
                           '"update_square_display" method.  ' \
                           'Must be a boolean instead ' \
                           '"hero_on_square" was Some_String'


def test_map_square_hero_on_square():
    test_map_square = MapSquare()
    test_map_square.update_square_display(True)
    assert test_map_square.__terrain_initial__.count('&') == 1


def test_map_square_has_been_visited():
    # Assert that if the square is visited and nothing else is set
    # The square will display its initial

    test_map_square = MapSquare()
    test_map_square.mark_visited()
    test_map_square.update_square_display()
    test_initial = test_map_square.__terrain__[0].upper()

    assert test_map_square.__terrain_initial__.count(test_initial) == 1


def test_map_square_has_not_been_visited():
    # Assert that if the square is visited and nothing else is set
    # The square will display its initial

    test_map_square = MapSquare()
    test_map_square.update_square_display()
    assert test_map_square.__terrain_initial__.count('?') == 1


def test_map_object_created(setup_large_map):
    assert setup_large_map.__size__ == 10
    assert isinstance(setup_large_map.__map__[1][1], MapSquare)
    assert isinstance(setup_large_map.__map__[10][10], MapSquare)


def test_map_object_reference_square_y_value_out_of_bounds(setup_large_map):
    with raises(KeyError) as e:
        setup_large_map.__map__[1][11]
    assert str(e.value) == '11'


def test_map_object_reference_square_x_value_out_of_bounds(setup_large_map):
    with raises(KeyError) as e:
        setup_large_map.__map__[11][1]
    assert str(e.value) == '11'


def test_map_no_size():
    with raises(TypeError) as e:
        Map()
    assert str(e.value) == \
        "Map Object Can Only Be Initialized with Integers.  " \
        "Tried to initialize with [None, None, None, None, None]"


def test_map_negative_size():
    with raises(ValueError) as e:
        Map(-2, 0, 0, obstacle_dict, 0, 0)
    assert str(e.value) == "Size must exist and be greater than one"


def test_map_non_integer():
    with raises(TypeError) as e:
        Map("10", 0, 0)
    print(e.value)
    assert str(e.value) == \
        "Map Object Can Only Be Initialized with Integers.  Tried to " \
        "initialize with ['10', 0, 0, None, None]"


def test_map_repr(setup_small_map):
    assert repr(setup_small_map) == 'A 2 by 2 Map'


def test_map_display(capsys, setup_small_map):
    setup_small_map.display()
    captured = capsys.readouterr()
    assert '*********' in captured.out


def test_map_move_into_square_invalid_no_values_set(setup_small_map):
    with raises(InvalidMapSquare) as e:
        setup_small_map.move_into_square()

    assert str(e.value) == 'Both "x" and "y" values must be set'


def test_map_move_into_square_invalid_x_too_big(setup_small_map):
    with raises(InvalidMapSquare) as e:
        setup_small_map.move_into_square(3, 1)

    assert str(e.value) == 'The values 3, 1 are invalid coordinates.  ' \
                           'Max size is 2x2.'


def test_map_move_into_square_invalid_y_too_big(setup_small_map):
    with raises(InvalidMapSquare) as e:
        setup_small_map.move_into_square(1, 3)

    assert str(e.value) == 'The values 1, 3 are invalid coordinates.  ' \
                           'Max size is 2x2.'


def test_map_move_into_square_invalid_x_too_small(setup_small_map):
    with raises(InvalidMapSquare) as e:
        setup_small_map.move_into_square(0, 1)

    assert str(e.value) == 'The values 0, 1 are invalid coordinates.  ' \
                           'Max size is 1x1.'


def test_map_move_into_square_invalid_y_too_small(setup_small_map):
    with raises(InvalidMapSquare) as e:
        setup_small_map.move_into_square(1, 0)

    assert str(e.value) == 'The values 1, 0 are invalid coordinates.  ' \
                           'Max size is 1x1.'


def test_map_move_into_square_valid_square(setup_medium_map):
    data = setup_medium_map.move_into_square(1, 2)

    assert isinstance(data, dict)

    sq = setup_medium_map.__map__[2][1]

    assert isinstance(sq, MapSquare)
    assert sq.__visited__
    assert sq.__terrain__ == data['terrain']
    assert sq.__money__ == data['money']


def test_map_neighbor_squares_initial_map(setup_medium_map):
    desired_set = {(0, 1), (1, 2), (0, 0), (2, 1),
                   (2, 0), (1, 1), (2, 2), (1, 0), (0, 2)}

    desired_with_binos = {(1, 3), (-1, 0), (3, 0), (2, 1),
                          (0, 3), (1, -1), (1, 2), (-1, 1),
                          (3, 3), (2, 2), (-1, -1), (1, 1),
                          (3, 2), (0, 0), (-1, 2), (2, -1),
                          (2, 3), (1, 0), (0, 1), (-1, 3),
                          (3, 1), (3, -1), (2, 0), (0, -1), (0, 2)}

    data = setup_medium_map.__hero_neighbors__()
    with_binos = setup_medium_map.__hero_neighbors__(True)
    assert data == desired_set
    assert with_binos == desired_with_binos


def test_map_neighbor_invalid_input(setup_medium_map):
    with raises(ValueError) as e:
        setup_medium_map.__hero_neighbors__('invalid_input')

    assert str(e.value) == 'Invalid data type passed to ' \
                           '"update_square_display" method.  ' \
                           'Must be a boolean.  "binoculars" ' \
                           'instead was invalid_input'


def test_map_confirm_qty_jewels(setup_large_map):
    count = 0
    for y in setup_large_map.__map__.values():
        for x in y.values():
            if x.get_square_details()['jewel']:
                count += 1

    assert count == 2


def test_map_confirm_qty_power_bars(setup_large_map):
    count = 0
    for y in setup_large_map.__map__.values():
        for x in y.values():
            if x.get_square_details()['power_bar']:
                count += 1

    assert count == 4


def test_map_confirm_qty_obstacle(setup_large_map):
    count = 0
    for y in setup_large_map.__map__.values():
        for x in y.values():
            if x.get_square_details()['obstacle']:
                count += 1

    assert count == 2


def test_map_confirm_qty_money_squares(setup_large_map):
    count = 0
    for y in setup_large_map.__map__.values():
        for x in y.values():
            if x.get_square_details()['money']:
                count += 1

    assert count == 2


def test_map_confirm_obstacle_names(setup_large_map):
    count = 0
    for y in setup_large_map.__map__.values():
        for x in y.values():
            if 'name' in x.get_square_details()['obstacle'] and \
                    'energy_cost' in x.get_square_details()['obstacle']:
                count += 1

    assert count == 2


def test_map_too_many_jewels():
    with raises(ValueError) as e:
        Map(3, 10, 0, obstacle_dict, 0, 0)

    assert str(e.value) == 'Number of objects to set ' \
                           'must be less than the total number of squares'


def test_map_too_few_jewels():
    with raises(ValueError) as e:
        Map(3, -3, 0, obstacle_dict, 0, 0)

    assert str(e.value) == 'Parameters must be greater than or equal to zero'


def test_map_too_many_obstacles():
    with raises(ValueError) as e:
        Map(3, 0, 10, obstacle_dict, 0, 0)

    assert str(e.value) == 'Number of objects to set ' \
                           'must be less than the total number of squares'


def test_map_too_few_obstacles():
    with raises(ValueError) as e:
        Map(3, 0, -3, obstacle_dict, 0, 0)

    assert str(e.value) == 'Parameters must be greater than or equal to zero'


def test_map_too_many_power_bars():
    with raises(ValueError) as e:
        Map(3, 0, 10, power_bar_qty=30, squares_with_money=0)

    assert str(e.value) == 'Number of objects to set ' \
                           'must be less than the total number of squares'


def test_map_too_few_power_bars():
    with raises(ValueError) as e:
        Map(3, 0, -3, power_bar_qty=-4, squares_with_money=0)

    assert str(e.value) == 'Parameters must be greater than or equal to zero'


def test_map_too_many_money_square():
    with raises(ValueError) as e:
        Map(3, 0, 10, power_bar_qty=30, squares_with_money=100)

    assert str(e.value) == 'Number of objects to set ' \
                           'must be less than the total number of squares'


def test_map_too_few_money_squares():
    with raises(ValueError) as e:
        Map(3, 0, -3, power_bar_qty=-4, squares_with_money=-5)

    assert str(e.value) == 'Parameters must be greater than or equal to zero'
