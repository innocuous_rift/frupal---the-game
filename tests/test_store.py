from frupal.store import Store
from frupal.settings import Settings
import pytest


@pytest.fixture()
def default_settings():
    settings = Settings()
    yield settings.tools


def test_store_can_be_initialized(default_settings):
    s = Store(default_settings)
    assert isinstance(s, Store)
    assert s.store_list == [('weed_whacker', 'weed whacker', 20.0),
                            ('jack_hammer', 'jack hammer', 30.0),
                            ('chain_saw', 'chain saw', 40.0),
                            ('pocket_knife', 'pocket knife', 5.0),
                            ('rope', 'rope', 1.0),
                            ('boat', 'boat', 50.0),
                            ('binoculars', 'binoculars', 50.0),
                            ('carrot', 'carrot', 3.0),
                            ('ear_plugs', 'ear plugs', 2.0)]


def test_store_update_store_list(default_settings):
    s = Store(default_settings)
    assert s.store_list == [('weed_whacker', 'weed whacker', 20.0),
                            ('jack_hammer', 'jack hammer', 30.0),
                            ('chain_saw', 'chain saw', 40.0),
                            ('pocket_knife', 'pocket knife', 5.0),
                            ('rope', 'rope', 1.0),
                            ('boat', 'boat', 50.0),
                            ('binoculars', 'binoculars', 50.0),
                            ('carrot', 'carrot', 3.0),
                            ('ear_plugs', 'ear plugs', 2.0)]
    s.tools.pop('weed_whacker')
    s.update_store_list()
    assert s.store_list == [('jack_hammer', 'jack hammer', 30.0),
                            ('chain_saw', 'chain saw', 40.0),
                            ('pocket_knife', 'pocket knife', 5.0),
                            ('rope', 'rope', 1.0),
                            ('boat', 'boat', 50.0),
                            ('binoculars', 'binoculars', 50.0),
                            ('carrot', 'carrot', 3.0),
                            ('ear_plugs', 'ear plugs', 2.0)]


def test_store_return_selected_tool(default_settings):
    s = Store(default_settings)
    tool, values = s.return_selected_tool(1)
    assert tool == 'weed_whacker'
    assert values == {'name': 'weed whacker', 'price': 20.0, 'energy_saved': 2}
    assert s.store_list == [('jack_hammer', 'jack hammer', 30.0),
                            ('chain_saw', 'chain saw', 40.0),
                            ('pocket_knife', 'pocket knife', 5.0),
                            ('rope', 'rope', 1.0),
                            ('boat', 'boat', 50.0),
                            ('binoculars', 'binoculars', 50.0),
                            ('carrot', 'carrot', 3.0),
                            ('ear_plugs', 'ear plugs', 2.0)]
