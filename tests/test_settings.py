from frupal.settings import Settings
from unittest.mock import patch


def test_setting_verify_settings():
    s = Settings()
    assert isinstance(s, Settings)
    assert isinstance(s.map_size, int)


def test_setting_valid_add_tool():
    s = Settings()
    user_input = [
        'Taco Truck',
        '500.00',
        '5'
    ]

    expected_entry = {
        'name': 'Taco Truck',
        'price': 500.00,
        'energy_saved': 5
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_tool()

    assert 'taco_truck' in s.tools
    assert expected_entry['name'] == s.tools['taco_truck']['name']
    assert expected_entry['price'] == s.tools['taco_truck']['price']
    assert expected_entry['energy_saved'] \
        == s.tools['taco_truck']['energy_saved']


def test_setting_invalid_add_tool():
    s = Settings()
    user_input = [
        'Taco Truck',
        'three',
        '500.00',
        'twenty five',
        '5'
    ]

    expected_entry = {
        'name': 'Taco Truck',
        'price': 500.00,
        'energy_saved': 5
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_tool()

    # Asserts that the valid values were added to tool and the invalid values
    # were not
    assert 'taco_truck' in s.tools
    assert expected_entry['name'] == s.tools['taco_truck']['name']
    assert expected_entry['price'] == s.tools['taco_truck']['price']
    assert expected_entry['energy_saved'] \
        == s.tools['taco_truck']['energy_saved']


def test_setting_add_tool_to_update_tool():
    s = Settings()
    user_input = [
        'Taco Truck',
        '500.00',
        '5'
    ]

    expected_entry = {
        'name': 'Taco Truck',
        'price': 500.00,
        'energy_saved': 5
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_tool()

    # Make sure taco truck was added to the tool list
    assert 'taco_truck' in s.tools
    assert expected_entry['name'] == s.tools['taco_truck']['name']
    assert expected_entry['price'] == s.tools['taco_truck']['price']
    assert expected_entry['energy_saved'] \
        == s.tools['taco_truck']['energy_saved']

    user_input = [
        'Taco Truck',
        'y',
        'Taco Truck',
        '2',
        '1000',
        'n'
    ]

    expected_entry = {
        'name': 'Taco Truck',
        'price': 1000.00,
        'energy_saved': 5
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_tool()

    # Make sure taco truck was added to the tool list
    assert 'taco_truck' in s.tools
    assert expected_entry['name'] == s.tools['taco_truck']['name']
    assert expected_entry['price'] == s.tools['taco_truck']['price']
    assert expected_entry['energy_saved'] \
        == s.tools['taco_truck']['energy_saved']


def test_remove_tool():
    s = Settings()

    user_input = [
        'jack hammer',
        '20.00',
        '3'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.add_tool()

    assert 'jack_hammer' in s.tools

    user_input = ['jack hammer']

    with patch('builtins.input', side_effect=user_input):
        s.remove_tool()

    assert 'jack_hammer' not in s.tools


def test_update_tool_add():
    s = Settings()

    hot = [
        'hammer of Thor',
        '20.00',
        '3'
    ]

    hot_expected = {
        'name': 'hammer of Thor',
        'price': 20.00,
        'energy_saved': 3
    }

    with patch('builtins.input', side_effect=hot):
        s.add_tool()

    assert 'hammer_of_thor' in s.tools

    user_input = [
        'ice cream cone',
        'y',
        'ice cream cone',
        '5.00',
        '2'
    ]

    icc_expected = {
        'name': 'ice cream cone',
        'price': 5.00,
        'energy_saved': 2
    }

    with patch('builtins.input', side_effect=user_input):
        s.update_tool()

    # assert ice cream cone has been added to s.tools and
    # hammer of Thor is still in s.tools

    assert 'ice_cream_cone' in s.tools
    assert icc_expected['name'] == s.tools['ice_cream_cone']['name']
    assert icc_expected['price'] == s.tools['ice_cream_cone']['price']
    assert icc_expected['energy_saved'] \
        == s.tools['ice_cream_cone']['energy_saved']

    assert 'hammer_of_thor' in s.tools
    assert hot_expected['name'] == s.tools['hammer_of_thor']['name']
    assert hot_expected['price'] == s.tools['hammer_of_thor']['price']
    assert hot_expected['energy_saved'] \
        == s.tools['hammer_of_thor']['energy_saved']


def test_invalid_update_tool():
    s = Settings()

    hot = [
        'hammer of Thor',
        '20.00',
        '3'
    ]

    hot_expected = {
        'name': 'hammer of Thor',
        'price': 20.00,
        'energy_saved': 3
    }

    with patch('builtins.input', side_effect=hot):
        s.add_tool()

    assert 'hammer_of_thor' in s.tools

    icc = [
        'ice cream cone',
        '5.00',
        '2'
    ]

    icc_expected = {
        'name': 'ice cream cone',
        'price': 5.00,
        'energy_saved': 2
    }

    with patch('builtins.input', side_effect=icc):
        s.add_tool()

    assert 'ice_cream_cone' in s.tools

    # try to update a tool not in tools
    user_input = [
        'random string',
        'n',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_tool()

    # Nothing is added and nothing is removed and all values
    # remain the same
    assert 'random_string' not in s.tools

    assert 'ice_cream_cone' in s.tools
    assert icc_expected['name'] == s.tools['ice_cream_cone']['name']
    assert icc_expected['price'] == s.tools['ice_cream_cone']['price']
    assert icc_expected['energy_saved'] \
        == s.tools['ice_cream_cone']['energy_saved']

    assert 'hammer_of_thor' in s.tools
    assert hot_expected['name'] == s.tools['hammer_of_thor']['name']
    assert hot_expected['price'] == s.tools['hammer_of_thor']['price']
    assert hot_expected['energy_saved'] \
        == s.tools['hammer_of_thor']['energy_saved']


def test_single_update_tool():
    s = Settings()

    hot = [
        'hammer of Thor',
        '20.00',
        '3'
    ]

    hot_expected = {
        'name': 'hammer of Thor',
        'price': 20.00,
        'energy_saved': 3
    }

    with patch('builtins.input', side_effect=hot):
        s.add_tool()

    assert 'hammer_of_thor' in s.tools

    icc = [
        'ice cream cone',
        '5.00',
        '2'
    ]

    icc_expected = {
        'name': 'new and improved ice cream cone',
        'price': 5.00,
        'energy_saved': 2
    }

    with patch('builtins.input', side_effect=icc):
        s.add_tool()

    assert 'ice_cream_cone' in s.tools

    # try to update a tool with a non-int as a field to update and
    # an int that is out of the index range before picking a
    # valid value to update
    # does not update multiple times
    user_input = [
        'ice cream cone',
        'not an int',
        '-5',
        '1',
        'new and improved ice cream cone',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_tool()

    # The name of the ice cream cone has been changed successfully
    # and no other fields have been modified

    assert 'ice_cream_cone' in s.tools
    assert icc_expected['name'] == s.tools['ice_cream_cone']['name']
    assert icc_expected['price'] == s.tools['ice_cream_cone']['price']
    assert icc_expected['energy_saved'] \
        == s.tools['ice_cream_cone']['energy_saved']

    assert 'hammer_of_thor' in s.tools
    assert hot_expected['name'] == s.tools['hammer_of_thor']['name']
    assert hot_expected['price'] == s.tools['hammer_of_thor']['price']
    assert hot_expected['energy_saved'] \
        == s.tools['hammer_of_thor']['energy_saved']


def test_multiple_update_tool():
    s = Settings()

    hot = [
        'hammer of Thor',
        '20.00',
        '3'
    ]

    hot_expected = {
        'name': 'hammer of Thor',
        'price': 15.00,
        'energy_saved': 3
    }

    with patch('builtins.input', side_effect=hot):
        s.add_tool()

    assert 'hammer_of_thor' in s.tools

    icc = [
        'ice cream cone',
        '5.00',
        '2'
    ]

    icc_expected = {
        'name': 'ice cream cone',
        'price': 5.00,
        'energy_saved': 8
    }

    with patch('builtins.input', side_effect=icc):
        s.add_tool()

    assert 'ice_cream_cone' in s.tools

    # Update multiple tools at once
    user_input = [
        'hammer of Thor',
        '2',
        '15.00',
        'y',
        'ice cream cone',
        '3',
        '8',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_tool()

    # The name of the ice cream cone has been changed successfully
    # and no other fields have been modified

    assert 'ice_cream_cone' in s.tools
    assert icc_expected['name'] == s.tools['ice_cream_cone']['name']
    assert icc_expected['price'] == s.tools['ice_cream_cone']['price']
    assert icc_expected['energy_saved'] \
        == s.tools['ice_cream_cone']['energy_saved']

    assert 'hammer_of_thor' in s.tools
    assert hot_expected['name'] == s.tools['hammer_of_thor']['name']
    assert hot_expected['price'] == s.tools['hammer_of_thor']['price']
    assert hot_expected['energy_saved'] \
        == s.tools['hammer_of_thor']['energy_saved']


def test_setting_valid_add_obstacle():
    s = Settings()

    user_input = [
        'Rabid Bunny',
        '5',
        'grenade',
        'n'
    ]

    expected_entry = {
        'name': 'Rabid Bunny',
        'energy_cost': 5,
        'associated_tools': ['grenade']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert expected_entry['name'] == s.obstacles['rabid_bunny']['name']
    assert expected_entry['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert expected_entry['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_setting_valid_add_obstacle_multi_tool():
    s = Settings()

    user_input = [
        'Rabid Bunny',
        '5',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    expected_entry = {
        'name': 'Rabid Bunny',
        'energy_cost': 5,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert expected_entry['name'] == s.obstacles['rabid_bunny']['name']
    assert expected_entry['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert expected_entry['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_setting_invalid_add_obstacle():
    s = Settings()
    user_input = [
        'Rabid Bunny',
        'five',
        '5',
        'grenade',
        'n'
    ]

    expected_entry = {
        'name': 'Rabid Bunny',
        'energy_cost': 5,
        'associated_tools': ['grenade']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert expected_entry['name'] == s.obstacles['rabid_bunny']['name']
    assert expected_entry['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert expected_entry['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_remove_obstacle():
    s = Settings()

    user_input = [
        'construction zone',
        '5',
        'ear plugs',
        'y',
        'jack hammer',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'construction_zone' in s.obstacles

    user_input = ['construction zone']

    with patch('builtins.input', side_effect=user_input):
        s.remove_obstacle()

    assert 'construction_zone' not in s.obstacles


def test_update_obstacle_add():

    s = Settings()

    # add a new obstacle
    user_input = [
        'Rabid Bunny',
        '5',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 5,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # attempts to update an obstacle that is not in
    # s.obstacles. responds 'y' when asked if we would
    # like to add it and provides the necessary info
    # to add the obstacle
    new_entry = [
        'Construction Zone',
        'y',
        'Construction Zone',
        '3',
        'ear plugs',
        'n',
    ]

    cz = {
        'name': 'Construction Zone',
        'energy_cost': 3,
        'associated_tools': ['ear plugs']
    }

    with patch('builtins.input', side_effect=new_entry):
        s.update_obstacle()

    # Assert rabid bunny is still in obstacles and
    # construction zone is also in obstacles now
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    assert 'construction_zone' in s.obstacles
    assert cz['name'] == s.obstacles['construction_zone']['name']
    assert cz['energy_cost'] == \
        s.obstacles['construction_zone']['energy_cost']
    assert cz['associated_tools'] \
        == s.obstacles['construction_zone']['associated_tools']


def test_update_obstacle_invalid_initial_input():
    s = Settings()

    # adds an obstacle
    user_input = [
        'Rabid Bunny',
        '5',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 5,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # attempts to update an obstacle not in the
    # s.obstacles. Chooses no to add the obstacle and
    # no to update a different obstacle.
    new_entry = [
        'Construction Zone',
        'n',
        'n',
    ]

    with patch('builtins.input', side_effect=new_entry):
        s.update_obstacle()

    # Assert rabid bunny is still in obstacles and
    # construction zone is not in obstacles
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    assert 'construction_zone' not in s.obstacles


def test_update_obstacle_update_name():
    s = Settings()

    user_input = [
        'Rabid Bunny',
        '5',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 5,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # Attempts to update construction zone which is not in
    # the list. Says yes to updating a different obstacle.
    # Feeds several invalid (edge case) arguments for selecting which
    # field to update before choosing the name field successfully.
    # Changes the name from Rabid Bunny to Tank.
    user_input = [
        'construction zone',
        'n',
        'y',
        'Rabid Bunny',
        '0',
        '4',
        '-1',
        '5',
        'name',
        '1',
        'Tank',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['name'] = 'Tank'

    # Assert rabid bunny is still in obstacles and
    # it's equal to the expected value
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_update_obstacle_update_energy_cost():
    s = Settings()

    user_input = [
        'Rabid Bunny',
        '2',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 2,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # Updates the energy cost for Rabid Bunny by first
    # attempting to set it to invalid values "-5" and five.
    # Since it cannot be a non-int or negative, the
    # energy cost is not updated until we input 5.
    user_input = [
        'Rabid Bunny',
        '2',
        '-5',
        '2',
        'five',
        '2',
        '5',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['energy_cost'] = 5

    # Assert rabid bunny is still in obstacles and
    # it's equal to the expected value
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_update_obstacle_add_associated_tools():
    s = Settings()

    user_input = [
        'Rabid Bunny',
        '2',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 2,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # Adds a new tool to the assoicated tool list.
    # First inputs several invalid fields for choosing
    # which tool option to select (0, -1, 4, and, one)
    # before finally inputting one and adding carrot
    # to the list of associated tools. We choose not to
    # update the tool list further or update any other
    # obstacles.
    user_input = [
        'Rabid Bunny',
        '3',
        '0',
        '-1',
        '4',
        'one',
        '1',
        'carrot',
        'n',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['associated_tools'].append('carrot')

    # Assert rabid bunny is still in obstacles and
    # it's equal to the expected value
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_update_obstacle_remove_associated_tools():
    s = Settings()

    # adds a new obstacle
    user_input = [
        'Rabid Bunny',
        '2',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 2,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # Attempts to update the associated tool list for
    # Rabid Bunny. Provides invalid input for which
    # tool to remove (0, -1, and 3 are out of range
    # (edge cases), and "two" is a string. Finally, 1 is
    # entered and "grenade" is removed from the assoicated
    # tool list. We choose not to update the tool
    # list further or update any other obstacles.
    user_input = [
        'Rabid Bunny',
        '3',
        '2',
        '0',
        '2',
        '-1',
        '2',
        '3',
        '2',
        'two',
        '2',
        '1',
        'n',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['associated_tools'] = ['dragon']

    # Assert rabid bunny is still in obstacles and
    # it's equal to the expected value
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_update_obstacle_update_associated_tools():
    s = Settings()

    # adds an obstacle
    user_input = [
        'Rabid Bunny',
        '2',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 2,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # This test attempts to update the associated tool
    # list in Rabid Bunny. When choosing which tool to
    # update, this tests several values which are out of
    # range (0, -1, and 3), all edge cases. 2 is finally
    # chosen and dragon is changed to carrot. We do not
    # choose to update additional tools or obstacles.
    user_input = [
        'Rabid Bunny',
        '3',
        '3',
        '0',
        '3',
        '-1',
        '3',
        '3',
        '3',
        '2',
        'carrot',
        'n',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['associated_tools'] = ['grenade', 'carrot']

    # Assert rabid bunny is still in obstacles and
    # it's equal to the expected value
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_update_obstacle_update_associated_tools_multi():
    s = Settings()

    user_input = [
        'Rabid Bunny',
        '2',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 2,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=user_input):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    # Attempts to update multiple tools in the associated
    # tool list for Rabid Bunny. Grenade is removed from
    # the tool list, and then we choose to update another
    # tool. Two invalid values are tried for picking
    # whether to update a tool or add a tool (since there
    # is now only one tool in the list, we do not have
    # the option to delete a tool). 0 and 3 are edge
    # cases and out of range. Finally we choose to add a
    # tool and carrot is added to the list of associated
    # tools. We choose not to add any more tools or
    # update any more obstacles.
    user_input = [
        'Rabid Bunny',
        '3',
        '2',
        '1',
        'y',
        '0',
        '3',
        '1',
        'carrot',
        'n',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['associated_tools'] = ['dragon', 'carrot']

    # Assert rabid bunny is still in obstacles and
    # it's equal to the expected value
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']


def test_update_obstacle_multi_obstacle():
    s = Settings()

    # add two obstacles
    rabid_bunny = [
        'Rabid Bunny',
        '2',
        'grenade',
        'y',
        'dragon',
        'n'
    ]

    rb = {
        'name': 'Rabid Bunny',
        'energy_cost': 2,
        'associated_tools': ['grenade', 'dragon']
    }

    with patch('builtins.input', side_effect=rabid_bunny):
        s.add_obstacle()

    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    construction_zone = [
        'Construction Zone',
        '3',
        'ear plugs',
        'n',
    ]

    cz = {
        'name': 'Construction Zone',
        'energy_cost': 3,
        'associated_tools': ['ear plugs']
    }

    with patch('builtins.input', side_effect=construction_zone):
        s.add_obstacle()

    assert 'construction_zone' in s.obstacles
    assert cz['name'] == s.obstacles['construction_zone']['name']
    assert cz['energy_cost'] == \
        s.obstacles['construction_zone']['energy_cost']
    assert cz['associated_tools'] \
        == s.obstacles['construction_zone']['associated_tools']

    # Attempting to update multiple obstacles. We first update
    # Rabid Bunny to remove the "grenade" from its list of
    # associated tools. Although we choose not to update the
    # tool list of Rabid Bunny any further, we choose to update
    # another obstacle and pick construction zone. We change
    # construction zone's energy_cost to 4 instead of 3. We choose
    # not to update any further obstacles.
    user_input = [
        'Rabid Bunny',
        '3',
        '2',
        '1',
        'n',
        'y',
        'construction zone',
        '2',
        '4',
        'n'
    ]

    with patch('builtins.input', side_effect=user_input):
        s.update_obstacle()

    rb['associated_tools'] = ['dragon']
    cz['energy_cost'] = 4

    # Assert both rabid_bunny and construction_zone are
    # still in obstacles and have been updated as expected
    assert 'rabid_bunny' in s.obstacles
    assert rb['name'] == s.obstacles['rabid_bunny']['name']
    assert rb['energy_cost'] == \
        s.obstacles['rabid_bunny']['energy_cost']
    assert rb['associated_tools'] \
        == s.obstacles['rabid_bunny']['associated_tools']

    assert 'construction_zone' in s.obstacles
    assert cz['name'] == s.obstacles['construction_zone']['name']
    assert cz['energy_cost'] == \
        s.obstacles['construction_zone']['energy_cost']
    assert cz['associated_tools'] \
        == s.obstacles['construction_zone']['associated_tools']
