from frupal.hero import Hero


def test_hero_created():
    test_hero = Hero('name', 5, 10)
    assert isinstance(test_hero.__name__, str)
    assert isinstance(test_hero.__money__, int)
    assert isinstance(test_hero.__energy__, int)
    assert test_hero.__jewels__ == 0
    assert not test_hero.__tools__
    assert test_hero.__location_y__ == 1
    assert test_hero.__location_y__ == 1


def test_check_energy():
    test_hero = Hero('name', 5, 10)
    energy = test_hero.check_energy()
    assert energy == 10


def test_check_jewels():
    test_hero = Hero('name', 5, 10)
    test_jewels = test_hero.check_jewels()
    assert test_jewels == 0


def test_update_energy():
    # checks if energy is being subtracted properly
    test_hero = Hero('name', 5, 5)
    updated_energy = test_hero.add_energy(5)
    assert updated_energy == 10
    assert test_hero.__energy__ == 10
    updated_energy_2 = test_hero.add_energy(4)
    assert updated_energy_2 == 14
    assert test_hero.__energy__ == 14


def test_update_energy_negative():
    # checks if adding energy works
    test_hero = Hero('name', 5, 0)
    negative_energy = test_hero.add_energy(-11)
    assert negative_energy == -11
    assert test_hero.__energy__ == -11


def test_update_money():
    # checks if money is subtracting from hero
    test_hero = Hero('name', 5, 0)
    updated_money = test_hero.add_money(100)
    assert updated_money == 105
    assert test_hero.__money__ == 105


def test_add_jewel():
    test_hero = Hero('name', 5, 0)
    jewels_added = test_hero.add_jewels(4)
    assert jewels_added == 4
    assert test_hero.__jewels__ == 4


def test_update_stats():
    test_hero = Hero('name', 5, 0)
    test_hero.update_stats(5, 120, 1, 2, 5)
    assert test_hero.__energy__ == -5
    assert test_hero.__money__ == 125
    assert test_hero.__jewels__ == 1
    assert test_hero.__location_x__ == 2
    assert test_hero.__location_y__ == 5


def test_buy_tool():
    test_hero = Hero('name', 100, 0)
    tools = {
        'axe': {'name': 'axe', 'price': 50, 'energy_saved': 5},
        'saw': {'name': 'saw', 'price': 40, 'energy_saved': 7}
    }
    test_catch = test_hero.buy_tool(tools['axe'])
    assert test_catch
    assert test_hero.__tools__['axe']['price'] == 50
    assert test_hero.__tools__['axe']['energy_saved'] == 5


# test insufficient funds returns false
def test_buy_tool_fail():
    test_hero = Hero('name', 5, 0)
    tools = {
        'axe': {'name': 'axe', 'price': 50, 'energy_saved': 5},
        'saw': {'name': 'saw', 'price': 40, 'energy_saved': 7}
    }
    test_catch = test_hero.buy_tool(tools['axe'])
    assert not test_catch
