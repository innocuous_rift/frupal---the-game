# Frupal - The Game
Frupal: the fun, magic-jewel seeking game for PSU's CS-300 class!

# Table of Contents
1. [Development Team](#development-team)
2. [Shared Google Drive Folder](#shared-google-drive-folder)
3. [PEP 8 Style Guide](#pep-8-style-guide)
4. [Sample Project](#sample-project)
5. [Markdown Guide](#markdown-guide)
6. [Installing Packages](#installing-packages)
7. [PyTest](#pytest)
8. [Running the Program](#running-the-program)

## Development Team

    Jane Seigman
    Robert Pelayo
    Joeseph Jacobs
    Yiwei Deng
    Alexandra (Allie) Hanson
    
## Shared Google Drive Folder:

https://docs.google.com/document/d/1BRErDXwwgkwEKTeNZYMOK5lqP_SEPyR_wuUyi1oFU2E/edit?usp=sharing


## PEP 8 Style Guide 
https://www.python.org/dev/peps/pep-0008/

From the command line type

    flake8

This will read in the exceptions in the .flake8 file and tell you issues to fix in your code

## Sample Project
https://docs.python-guide.org/writing/structure/

## Markdown Guide
https://www.markdownguide.org/basic-syntax/


## Installing Packages
To install all the packages do a following:

    pip install --upgrade -r requirements.txt

## PyTest
To run pytest, go to the root of the project, run the following command

    pytest tests

To run pytest and get a coverage report, from the root of the project run

    pytest --cov-report term-missing --cov frupal tests

This command will display a coverage report to the screen with how much of the code is tested

## Running the Program
To run the program, from the main directory execute

    python frupal